import json
import os
from dataclasses import dataclass
from typing import Any


class InvalidGeojsonKey(Exception):
    ...


@dataclass(frozen=True)
class DataToStoreInDB:
    trace_name: str
    customer_name: str
    farm_name: str
    parcel_crop_name: str
    parcel_geometry: dict[str, Any]


class DataDigitalizationManager:
    TEMP_GEOJSON_RESOURCE = f"{os.path.dirname(__file__)}/HolmesT.geojson"

    @staticmethod
    def get_geojson(*, file_path: str) -> dict[str, Any]:
        """
        Only for test propouses.
        TODO: Use the real data provided by the public MOM API.
        """
        with open(file=file_path) as file_:
            data = json.load(file_)

        return data

    @staticmethod
    def check_keys(*, keys: list, keys_to_check: tuple[str]):
        for key_to_check in keys_to_check:
            if key_to_check in keys:
                continue

            raise InvalidGeojsonKey(f'"{key_to_check}" key does not exist in GeoJson')

    def get_data_to_store_in_db(self, *, data=dict[str, Any]) -> DataToStoreInDB:
        self.check_keys(keys=data.keys(), keys_to_check=("features",))

        features = data["features"][0]
        self.check_keys(keys=features.keys(), keys_to_check=("properties", "geometry"))

        properties = features["properties"]
        data_to_store_in_db = DataToStoreInDB(
            trace_name=properties["trace_name"],
            customer_name=properties["customer_name"],
            farm_name=properties["field_name"],
            parcel_crop_name=properties["type"],
            parcel_geometry=features["geometry"],
        )
        return data_to_store_in_db

    @staticmethod
    def store_data_in_db(*, data_to_store_in_db: DataToStoreInDB) -> None:
        """
        TODO: use the DB connection and store the data in these tables(table_name.field_name):
            - trace.name = trace_name
            - user.name = customer_name
            - farm.name = farm_name
            - parce.geometry= parcel_geometry
            - parcel__parcel_crop.name = parcel_crop_name
        """
        print(data_to_store_in_db)

    def execute(self) -> None:
        data = self.get_geojson(file_path=self.TEMP_GEOJSON_RESOURCE)
        data_to_store_in_db = self.get_data_to_store_in_db(data=data)
        self.store_data_in_db(data_to_store_in_db=data_to_store_in_db)


if __name__ == "__main__":
    data_digitalization_manager = DataDigitalizationManager()
    data_digitalization_manager.execute()
